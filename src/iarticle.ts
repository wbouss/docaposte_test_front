interface Iarticle {
  id: number;
  title: string;
  content: string;
}
