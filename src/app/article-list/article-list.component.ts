import { Component, OnInit } from '@angular/core';
import {ArticleService} from '../services/article.service';

@Component({
  selector: 'app-article-list',
  templateUrl: './article-list.component.html',
  styleUrls: ['./article-list.component.css']
})
export class ArticleListComponent implements OnInit {

  private listArticles;

  constructor(private articleService: ArticleService) {
    this.articleService.getArticles().subscribe( value => {
      this.listArticles = JSON.parse(value);
    });
  }

  ngOnInit() {

  }

}
