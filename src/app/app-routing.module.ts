import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {TestComponent} from './test/test.component';
import {ArticleListComponent} from './article-list/article-list.component';
import {NewArticleComponent} from './new-article/new-article.component';
import {ArticlleDetailComponent} from "./articlle-detail/articlle-detail.component";

const routes: Routes = [
  { path: '', component: ArticleListComponent},
  { path: 'articles/create', component: NewArticleComponent},
  { path: 'articles/detail/:slug', component: ArticlleDetailComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
