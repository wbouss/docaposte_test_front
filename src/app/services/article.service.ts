import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ArticleService {

  private urlBase = 'http://127.0.0.1:8000/';

  constructor(private http: HttpClient) { }

  getArticles(): Observable<any> {
    return this.http.get( this.urlBase + 'articles');
  }

  getArticle(slug: string): Observable<any> {
    return this.http.get( this.urlBase + 'articles/' + slug );
  }

  create( article: any): Observable<any> {
    return this.http.post( this.urlBase + 'articles', JSON.stringify(article));
  }
}
