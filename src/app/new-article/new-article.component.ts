import { Component, OnInit } from '@angular/core';
import {ArticleService} from '../services/article.service';
import {FormBuilder, Validators} from '@angular/forms';

@Component({
  selector: 'app-new-article',
  templateUrl: './new-article.component.html',
  styleUrls: ['./new-article.component.css']
})
export class NewArticleComponent implements OnInit {
  checkoutForm;
  submitted = false;
  registered = false;
  constructor(private articleService: ArticleService,
              private formBuilder: FormBuilder
  ) {
    this.checkoutForm = this.formBuilder.group({
      title: ['' , Validators.required],
      content: ['' , Validators.required]
    });
  }

  ngOnInit() {
  }

  get g() { return this.checkoutForm.controls; }

  onSubmit(value: any): void {
    this.registered = false;
    this.submitted = true;

    if (this.checkoutForm.invalid) {
      return;
    }
    this.articleService.create(value).subscribe( value => {
      this.registered = true;
    });
  }

}
