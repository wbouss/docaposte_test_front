import { Component, OnInit } from '@angular/core';
import {ArticleService} from "../services/article.service";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-articlle-detail',
  templateUrl: './articlle-detail.component.html',
  styleUrls: ['./articlle-detail.component.css']
})
export class ArticlleDetailComponent implements OnInit {

  article: Iarticle;

  constructor(articleService: ArticleService, route: ActivatedRoute) {
    articleService.getArticle(route.snapshot.params.slug).subscribe( value => {
      this.article = JSON.parse(value);
    });
  }

  ngOnInit() {
  }

}
