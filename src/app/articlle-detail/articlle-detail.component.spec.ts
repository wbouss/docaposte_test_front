import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArticlleDetailComponent } from './articlle-detail.component';

describe('ArticlleDetailComponent', () => {
  let component: ArticlleDetailComponent;
  let fixture: ComponentFixture<ArticlleDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArticlleDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArticlleDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
